<?php
function inputJumlahBaris()
{
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        return isset($_POST['jumlah_baris']) ? (int)$_POST['jumlah_baris'] : 0;
    }
    return 0;
}

$jumlahBaris = inputJumlahBaris();
?>

<!-- HTML form to get user input -->
<form method="post" action="">
    <H1> Soal 1</H1>
    <label for="jumlah_baris">Masukkan jumlah baris:</label>
    <input type="number" name="jumlah_baris" id="jumlah_baris" value="<?php echo $jumlahBaris; ?>">
    <input type="submit" value="Submit">
</form>

<?php
if ($jumlahBaris > 0) {
    echo "<table border='1'>\n";

    // Loop untuk setiap baris
    for ($i = $jumlahBaris; $i >= 1; $i--) {
        // Hitung total angka
        $totalAngka = $i * ($i + 1) / 2;
        // Baris untuk total angka
        echo "  <tr>\n";
        echo "    <td colspan='$jumlahBaris'>Total: $totalAngka</td>\n";
        echo "  </tr>\n";

        // Baris untuk urutan penjumlahan
        echo "  <tr>\n";

        for ($j = $i; $j >= 1; $j--) {
            echo "    <td>" . ($j) . "</td>\n";
        }

        echo "  </tr>\n";
    }

    // Selesai tabel
    echo "</table>\n";
}
?>