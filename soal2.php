<?php
session_start();

// Inisialisasi variabel data jika belum ada
if (!isset($_SESSION['data'])) {
    $_SESSION['data'] = [];
}

// Jika form sudah disubmit
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    // Menyimpan data dari form saat ini ke dalam array
    if ($_SESSION['step'] == 1) {
        $_SESSION['data']['nama'] = $_POST['nama'];
    } elseif ($_SESSION['step'] == 2) {
        $_SESSION['data']['umur'] = $_POST['umur'];
    } elseif ($_SESSION['step'] == 3) {
        $_SESSION['data']['hobi'] = $_POST['hobi'];
    }

    // Memperbarui langkah saat ini
    $_SESSION['step']++;

    // Jika sudah mencapai langkah 4, tampilkan hasil input
    if ($_SESSION['step'] > 3) {
        unset($_SESSION['step']); // Hapus langkah dari session
        echo "Hasil Inputan:<br>";
        foreach ($_SESSION['data'] as $key => $value) {
            echo ucfirst($key) . ": " . $value . "<br>"; // Menampilkan hasil input
        }
        exit; // Keluar dari skrip setelah menampilkan hasil input
    }
}

// Tampilkan form sesuai dengan langkah saat ini
if (!isset($_SESSION['step'])) {
    $_SESSION['step'] = 1; // Set langkah awal jika belum diset
}

if ($_SESSION['step'] == 1) {
?>
    <form method='post'>Nama Anda: <input type='text' name='nama'><br><input type='submit' value='Submit'></form>
<?php
} elseif ($_SESSION['step'] == 2) {
?>
     <form method='post'>Umur Anda: <input type='text' name='umur'><br><input type='submit' value='Submit'></form>
<?php
} elseif ($_SESSION['step'] == 3) {
?>
     <form method='post'>Hobi Anda: <input type='text' name='hobi'><br><input type='submit' value='Submit'></form>
<?php
}

?>