<?php
// Koneksi ke database
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "testdb";

$conn = new mysqli($servername, $username, $password, $dbname);
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

// Query untuk menampilkan data person beserta hobinya
$sql = "SELECT person.nama, person.alamat, GROUP_CONCAT(hobi.hobi SEPARATOR ', ') AS hobinya 
        FROM person 
        LEFT JOIN hobi ON person.id = hobi.person_id 
        GROUP BY person.id";

// Pengecekan jika form search telah disubmit
if ($_SERVER["REQUEST_METHOD"] == "POST" && isset($_POST["search"])) {
    $nama = $_POST["nama"];
    $alamat = $_POST["alamat"];
    $conditions = [];

    if (!empty($nama)) {
        $conditions[] = "person.nama LIKE '%$nama%'";
    }

    if (!empty($alamat)) {
        $conditions[] = "person.alamat LIKE '%$alamat%'";
    }

    $whereClause = implode(" OR ", $conditions);

    $sql = "SELECT person.nama, person.alamat, GROUP_CONCAT(hobi.hobi SEPARATOR ', ') AS hobinya 
            FROM person 
            LEFT JOIN hobi ON person.id = hobi.person_id 
            WHERE $whereClause 
            GROUP BY person.id";
}

$result = $conn->query($sql);
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Data Person & Hobi</title>
</head>

<body>
    <h1>Data Person & Hobi</h1>

    <!-- Tabel untuk menampilkan data -->
    <table border="1">
        <tr>
            <th>Nama</th>
            <th>Alamat</th>
            <th>Hobi</th>
        </tr>
        <?php
        if ($result->num_rows > 0) {
            // Output data dari setiap row
            while ($row = $result->fetch_assoc()) {
                echo "<tr>";
                echo "<td>" . $row["nama"] . "</td>";
                echo "<td>" . $row["alamat"] . "</td>";
                echo "<td>" . $row["hobinya"] . "</td>";
                echo "</tr>";
            }
        } else {
            echo "<tr><td colspan='3'>Tidak ada data yang ditemukan</td></tr>";
        }
        ?>
    </table>
    <br>
    <!-- Form pencarian -->
    <form method="post">
        <label for="nama">Cari Nama:</label>
        <input type="text" id="nama" name="nama">
        <br>
        <label for="alamat">Cari Alamat:</label>
        <input type="text" id="alamat" name="alamat">
        <br>
        <button type="submit" name="search">Search</button>
    </form>

</body>

</html>

<?php
// Tutup koneksi
$conn->close();
?>